name := "processor-michelin"

version := "1.0"

scalaVersion := "2.11.11"

libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.4.4"

Compile/mainClass := Some("adriver.Main")
