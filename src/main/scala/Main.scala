package adriver

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._

object Main {
	
  def main(args: Array[String]) {

     val mArgs = {

      val argsList = args.toList
      val pattern =  "--(.*)".r

      def parseRec(map: Map[String, String], list: List[String]): Map[String, String] = {
                        
        list match {
          case  pattern(a) :: b :: tail => parseRec(map ++ Map(a -> b), tail)
          case a :: tail => map
          case Nil => map
        }
      }

      parseRec(Map(), argsList)
    }

    val startTs = unix_timestamp(lit(s"${mArgs("year")}-${mArgs("month")}-${mArgs("day")} 00:00:00")).cast("timestamp")
    val endTs = unix_timestamp(lit(s"${mArgs("year")}-${mArgs("month")}-${mArgs("day")} 23:59:59")).cast("timestamp")

  	val spark = SparkSession
  		.builder()
  		.appName("processor-michelin")
  		.config("hive.metastore.client.factory.class", "com.amazonaws.glue.catalog.metastore.AWSGlueDataCatalogHiveClientFactory")
  		.config("hive.exec.dynamic.partition", "true")
  		.config("hive.exec.dynamic.partition.mode", "nonstrict")
  		.enableHiveSupport()
  		.getOrCreate()

    import spark.implicits._

    val dfEvents = spark
      .read
      .json(s"s3://adriver.data.raw/MICHELIN/events/*/${mArgs("year")}/${mArgs("month")}/${mArgs("day")}/*")
      .withColumn("VehiclePosition", explode('VehiclePosition))
      .withColumn("VIN", $"VehiclePosition.VIN")
      .withColumn("timestamp", $"VehiclePosition.GNSSPosition.PositionDateTime")
      .withColumn("timestamp", to_timestamp('timestamp))
      .withColumn("latitude", $"VehiclePosition.GNSSPosition.Latitude")
      .withColumn("longitude", $"VehiclePosition.GNSSPosition.Longitude")
      .filter("VIN != 'NOVIN'")
      .withColumn("year", lit(mArgs("year")))
      .withColumn("month", lit(mArgs("month")))
      .withColumn("day", lit(mArgs("day")))
      .withColumn("provider", lit("MICHELIN"))
      .withColumn("haulier", split(input_file_name,"/")(5))
      .filter('timestamp >= startTs && 'timestamp <= endTs)
      .filter("latitude != 0 or longitude != 0")
      .groupBy('year, 'month, 'day, 'provider, 'haulier, 'VIN, 'timestamp)
      .agg(first('latitude) as "latitude", first('longitude) as "longitude")

    val dfVehicles = spark
      .read
      .json(s"s3://adriver.data.raw/MICHELIN/vehicles/*/${mArgs("year")}/${mArgs("month")}/${mArgs("day")}/*")
      .withColumn("Vehicle", explode('Vehicle))
      .withColumn("VIN", $"Vehicle.VIN")
      .filter("VIN != 'NOVIN'")
      .withColumn("immat", upper(regexp_replace(split($"Vehicle.CustomerVehicleName", "/")(1), "[ -]", "")))
      .withColumn("haulier", split(input_file_name,"/")(5))

    var sqlHaulier = s"select haulier_name as haulier, haulier_salesforce_ref from adriver.api_keys"
    val dfHaulier = spark.sql(sqlHaulier)
    
    dfEvents
        .join(dfVehicles, Seq("haulier", "VIN"))
        .join(dfHaulier, Seq("haulier"))
        .select("haulier", "immat", "timestamp", "latitude", "longitude", "haulier_salesforce_ref", "year", "month", "day", "provider")
        .write
        .format("parquet")
        .mode("overwrite")
        .insertInto("adriver.coordinates")

    spark.stop()
  }
}
